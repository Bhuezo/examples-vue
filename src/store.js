import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { resolve } from 'any-promise'

Vue.use(Vuex)
const store = new Vuex.Store({
  state: {
    count:  0,
    load: false,
    color: false
  },
  mutations: {
    increment(state, payload = {}){
      state.count += payload.number || 1;
    },
    decrement(state, payload = {}){
      state.count -= payload.number || 1
    }
  },
  getters:{
    getDouble(state, getters) {
      return state.count * 2
    }
  },
  actions: {
    incrementAsync (context, payload) {
      context.state.color = true
      context.state.load = true
      return new Promise((res,rej) => {
        setTimeout(()=>{
          context.commit('increment', payload)
          console.log('Action completed')
          context.state.load = false
          res()
        }, 2000)
      })
    },
    decrementAsync (context, payload) {
      context.state.color = false
      context.state.load = true
      return new Promise((res,rej) => {
        setTimeout(()=>{
          context.commit('decrement', payload)
          context.state.load = false
          console.log('Action completed')
          res()
        }, 2000)
      })
    }
  }
})

export default store