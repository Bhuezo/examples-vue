import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/qr',
      name: 'qr',
      component: function () { 
        return import(/* webpackChunkName: "about" */ './views/Qr.vue')
      }
    },
    {
      path: '/withvuex',
      name: 'withvuex',
      component: function () { 
        return import(/* webpackChunkName: "about" */ './views/WithVuex.vue')
      }
    },
    {
      path: '/chat',
      name: 'chat',
      component: function () { 
        return import(/* webpackChunkName: "about" */ './views/Chat.vue')
      }
    },
    {
      path: '/appmovil',
      name: 'appmovil',
      component: function () { 
        return import(/* webpackChunkName: "about" */ './views/AppMovil.vue')
      }
    },
    {
      path: '/snape',
      name: 'snape',
      component: function () { 
        return import(/* webpackChunkName: "about" */ './views/Snape.vue')
      }
    },
  ]
})
